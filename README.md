**ITESOFT mailing test**
This is the ITESOFT mailing Test 

---

## Clone a repository

For clone repository please use the following URL : git clone https://bitbucket.org/embdsftg/itesoft_mail_test.git

---

## Install and launch project

To install and launch the project please execute the following commands on a terminal on the folder project 

1. Install project => **mvn clean compile assembly:single**
2. Launch project => **java -jar itesoft-mail-test-jar-with-dependencies.jar**

## NOTES
The mail connections are configured on application.properties