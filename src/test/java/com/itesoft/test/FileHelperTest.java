package com.itesoft.test;

import org.junit.Assert;
import org.junit.Test;

import com.itesoft.test.util.ApplicationHelper;

public class FileHelperTest {
	
	/* Email validator tests */
	
	@Test
	public void validEmail() {
		Assert.assertTrue(ApplicationHelper.isValidEmail("valid@email.fr"));
	}
	
	@Test
	public void noDomainEmail() {
		Assert.assertFalse(ApplicationHelper.isValidEmail("valid@"));
	}
	
	@Test
	public void noAtEmailSeparator() {
		Assert.assertFalse(ApplicationHelper.isValidEmail("validemail.fr"));
	}
	
	/**
	 * We assume that an email should have always the country definition
	 */
	@Test
	public void invalidCountryRegionEmail() {
		Assert.assertFalse(ApplicationHelper.isValidEmail("valid@email"));
	}

}
