package com.itesoft.test.models;

public enum EmailProtocolEnum {
	
	POP3, IMAP;
	
	private static final String COMMON_PROTOCOL_KEY = ".mail.protocol.";
	
	@Override
	public String toString() {
		return name().toLowerCase();
	}
	
	public String getStorePropertyKey() {
		return toString() + ".store";
	}
	
	public String getHostPropertyKey() {
		return toString() + COMMON_PROTOCOL_KEY + "host";
	}
	
	public String getPortPropertyKey() {
		return toString() + COMMON_PROTOCOL_KEY + "port";
	}
	
	public String getUserPropertyKey() {
		return toString() + COMMON_PROTOCOL_KEY + "user";
	}
	
	public String getPasswordPropertyKey() {
		return toString() + COMMON_PROTOCOL_KEY + "password";
	}

}
