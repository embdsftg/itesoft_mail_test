package com.itesoft.test.models;

/**
 * The Email Filters class for store mail filters values
 * 
 * @author embds
 *
 */
public class EmailFilter {

	private final String emitterEmail;
	private final boolean attachments;
	private final String subjectText;

	/**
	 * @param emitterEmail
	 * @param attachments
	 * @param subjectText
	 */
	public EmailFilter(String emitterEmail, boolean attachments, String subjectText) {
		this.emitterEmail = emitterEmail;
		this.attachments = attachments;
		this.subjectText = subjectText;
	}

	/**
	 * @return the emitterEmail
	 */
	public String getEmitterEmail() {
		return emitterEmail;
	}

	/**
	 * @return the attachments
	 */
	public boolean withAttachments() {
		return attachments;
	}

	/**
	 * @return the subjectText
	 */
	public String getSubjectText() {
		return subjectText;
	}

}
