package com.itesoft.test.models;

/**
 * The Summary class to store the summary details
 * 
 * @author embds
 *
 */
public class Summary {
	
	private Long id;
	private String subject;
	
	// Default constructor only for Jackson mapping
	private Summary() {
		
	}
	
	/**
	 * @param id The incremental ID
	 * @param subject The email subject
	 */
	public Summary(final Long id, final String subject) {
		this.id = id;
		this.subject = subject;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	
}
