package com.itesoft.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;

import com.itesoft.test.models.EmailFilter;
import com.itesoft.test.models.Summary;
import com.itesoft.test.util.ApplicationHelper;
import com.itesoft.test.util.EmailHelper;
import com.itesoft.test.util.FilesHelper;

/**
 * This Application read all emails from servers and store it on local computer
 * 
 * @author embds
 * @see https://www.oracle.com/technetwork/java/javamail-1-149769.pdf
 */
public class MailTestApplication {
	
	private static final Logger LOGGER = Logger.getLogger(MailTestApplication.class.getName());
	
	private static final String BASE_PATH_DATA_LOCATION = "\\itesoft_data\\";
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// ask filters to user
		String emitterEmailFilter = "";
		System.out.println("Fill in the required emitter email : ");
		// required emitterEmail filter
		while ("".equals(emitterEmailFilter.trim())) {
			String userResponse = sc.nextLine();
			if (ApplicationHelper.isValidEmail(userResponse)) {
				emitterEmailFilter = userResponse;
			}else {
				System.out.println("The filled emitter email sin't valid! Please insert a valid email");
			}
		}
		// Email Attachments filter
        Boolean attachments = null;
        System.out.println("Do you want email only with attachments? (y/n)");
        while (Objects.isNull(attachments)) {
			String userResponse = sc.nextLine();
			if ("y".equalsIgnoreCase(userResponse)) {
				attachments = true;
			}else if("n".equalsIgnoreCase(userResponse)) {
				attachments = false;
			}else {
				System.out.println("Only the responses yes(y) or no(n) are accepted");
			}
		}
        // Subject text filter
        String subjectFilter = "";
        System.out.println("Insert text for subject research (leave blank for no filter):");
        subjectFilter = sc.nextLine();
		sc.close();
		System.out.println("loading data...");
		// Start procedure to create .eml, .txt and attachements files by email and unique summary.json
        try {
        	EmailFilter filters = new EmailFilter(emitterEmailFilter, attachments, subjectFilter);
	        List<Message> filteredEmails = EmailHelper.getFilteredEmailsFromAllServers(filters);
	        
	        List<Summary> summaryContent = null;
	        Long incrementalEmailId = null;
	        String cleanBaseUri = "";
	        File basePath = null;
	        for (Message email : filteredEmails) {
	        	// Sanitize the path (remove unauthorized charaters)
	        	final String sanitizeEmailFolderName = ApplicationHelper.sanitizeEmailFolderName(EmailHelper.getEmailEmitter(email));
	        	cleanBaseUri = BASE_PATH_DATA_LOCATION + sanitizeEmailFolderName;
		        basePath = new File(cleanBaseUri);
		        // Create directory if not exists and throws exception if not created
		        if (!basePath.exists()) {
			        if (!basePath.mkdirs()) {
			        	throw new InvalidPathException(cleanBaseUri, "It seems you haven't rights to create the directory to save files! - path");
			        }
		        }

		        // if incrementalId is null then first iteration and should initialize vars
		        if (Objects.isNull(incrementalEmailId)) {
			        System.out.println("Saving data...");
			        // get max number file from filtered .txt files on user path or the ID 0 by default and increment by 1
		        	incrementalEmailId = Files.list(Paths.get(cleanBaseUri))
			                .filter(Files::isRegularFile).filter(ApplicationHelper::isTxtFile).mapToLong(ApplicationHelper::mapToLong).max().orElse(0) + 1;
			        // get the summary.json content if exists
		        	summaryContent = FilesHelper.getSummaryContent(cleanBaseUri);
		        }
	        	
		        String fileName = cleanBaseUri + FilesHelper.PATH_SEPARATOR + incrementalEmailId.toString();

		        FilesHelper.writeEmlFile(fileName, email);
		        FilesHelper.writeTxtFile(fileName, email);
		        FilesHelper.writeAttachment(fileName, EmailHelper.getEmailAttachments(email));

		        // add email details to summary collections
		        summaryContent.add(new Summary(incrementalEmailId, EmailHelper.getEmailSubject(email)));
		        incrementalEmailId++;
			}
	        if (Objects.isNull(basePath)) {
	        	System.out.println("None email has been found for your search criteria!");
	        }else {
		        FilesHelper.writeSummaryFile(cleanBaseUri, summaryContent);
		        System.out.println("Files are been created under: " + basePath.getAbsolutePath());
	        }
		} catch (MessagingException  | IOException e) {
			LOGGER.log(Level.SEVERE, "unexpected error has been throw", e);
		}
	}
	
}
