package com.itesoft.test.util;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.AccessException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;

import com.itesoft.test.models.EmailFilter;
import com.itesoft.test.models.EmailProtocolEnum;

public class EmailHelper {

	private static final Logger LOGGER = Logger.getLogger(EmailHelper.class.getName());

	private static final String MIME_TYPE_TXT = "text/plain";
	private static final String MIME_TYPE_MULTIPART_ALL = "multipart/*";
	private static final String MIME_TYPE_TXT_HTML = "text/html";

	private static final String PROPERTIES_FILE_NAME = "application.properties";

	private static final String FOLDER_READ_MAILS = "INBOX";

	private static Properties APPLICATION_PROPERTIES = null;

	/**
	 * Get email's from servers and execute filter on list
	 * 
	 * @param filters The filters for apply on list of retrieved emails
	 * @return The filtered emails list
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static List<Message> getFilteredEmailsFromAllServers(final EmailFilter filters)
			throws MessagingException, IOException {
		List<Message> mergedServerMessages = new ArrayList<>();
		// Get the emails from servers
		mergedServerMessages.addAll(Arrays.asList(readInboxEmailByProtocol(EmailProtocolEnum.POP3)));
		mergedServerMessages.addAll(Arrays.asList(readInboxEmailByProtocol(EmailProtocolEnum.IMAP)));

		return mergedServerMessages.stream().filter(email -> validFilterEmailAssertion(email, filters))
				.collect(Collectors.toList());
	}

	/**
	 * Get properties files if not already loaded
	 * 
	 * @return The {@value #PROPERTIES_FILE_NAME} file
	 * @throws IOException
	 */
	private static Properties getApplicationProperties() throws IOException {
		if (Objects.isNull(APPLICATION_PROPERTIES)) {
			APPLICATION_PROPERTIES = loadApplicationProperties();
		}
		return APPLICATION_PROPERTIES;
	}

	/**
	 * Get the remote email's from the folder {@value #FOLDER_READ_MAILS} for the provided protocol
	 * 
	 * @param protocol The {@code EmailProtocolEnum} type protocol
	 * @return The list of email on
	 * @throws IOException
	 */
	private static Message[] readInboxEmailByProtocol(final EmailProtocolEnum protocol) throws IOException {
		// create the session
		Session mailSession = Session.getDefaultInstance(readMailProperties(protocol));
		Store store;
		try {
			// create the store object and connect to the server
			store = mailSession.getStore(getApplicationProperties().getProperty(protocol.getStorePropertyKey()));
			store.connect(getApplicationProperties().getProperty(protocol.getHostPropertyKey()),
					getApplicationProperties().getProperty(protocol.getUserPropertyKey()),
					getApplicationProperties().getProperty(protocol.getPasswordPropertyKey()));
			// create the folder object and open it
			Folder emailFolder = store.getFolder(FOLDER_READ_MAILS);
			emailFolder.open(Folder.READ_ONLY);
			// return all email's
			return emailFolder.getMessages();
		} catch (MessagingException e) {
			LOGGER.log(Level.SEVERE, "unexpected error reading emails for protocol: " + protocol, e);
		}
		// case where an error occurred reading emails
		return new Message[0];
	}

	/**
	 * Get properties email for session creation
	 * 
	 * @param protocolKey The {@code EmailProtocolEnum} protocol
	 * @return The session mail properties
	 * @throws IOException
	 */
	private static Properties readMailProperties(final EmailProtocolEnum protocolKey) throws IOException {
		Properties properties = new Properties();
		properties.put("mail.store.protocol",
				getApplicationProperties().getProperty(protocolKey.getStorePropertyKey()));
		properties.put("mail." + protocolKey + ".host",
				getApplicationProperties().getProperty(protocolKey.getHostPropertyKey()));
		properties.put("mail." + protocolKey + ".port",
				getApplicationProperties().getProperty(protocolKey.getPortPropertyKey()));
		properties.put("mail." + protocolKey + ".starttls.enable", "true");
		return properties;
	}

	/**
	 * Load the {@value #PROPERTIES_FILE_NAME} from application resources
	 * 
	 * @return The Properties Object if loaded successfully
	 * @throws IOException
	 */
	private static Properties loadApplicationProperties() throws IOException {
		Properties applicationProperties = new Properties();
		InputStream input = null;
		try {
			input = EmailHelper.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME);
			if (Objects.isNull(input)) {
				throw new AccessException("Unable to find " + PROPERTIES_FILE_NAME);
			}
			// load a properties file from class path
			applicationProperties.load(input);
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, "Unexpected error getting " + PROPERTIES_FILE_NAME, ex);
			throw ex;
		} finally {
			if (Objects.nonNull(null)) {
				try {
					input.close();
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE, "Unexpected error closing input stream!", e);
				}
			}
			if (Objects.isNull(applicationProperties)) {
				throw new AccessException("Unable to load " + PROPERTIES_FILE_NAME);
			}
		}
		return applicationProperties;
	}

	/**
	 * check if Email respects the filters criteria
	 * 
	 * @param email
	 *            The email to inspect
	 * @param filters
	 *            The filters to be checked
	 * @return true if filters are respected, false instead
	 */
	private static boolean validFilterEmailAssertion(final Message email, final EmailFilter filters) {
		boolean validAssertion = false;
		// Check if emitter email
		try {
			validAssertion = getEmailEmitter(email).equalsIgnoreCase(filters.getEmitterEmail());
			// If attachments false then ignore this filter
			if (validAssertion && filters.withAttachments()) {
				validAssertion = !getEmailAttachments(email).isEmpty();
			}
			// If subjectFilter empty then ignore this filter
			if (validAssertion && !"".equals(filters.getSubjectText())) {
				validAssertion = getEmailSubject(email).contains(filters.getSubjectText());
			}
			return validAssertion;
		} catch (MessagingException | IOException e) {
			LOGGER.log(Level.SEVERE, "unexpected error checking filters", e);
			return false;
		}
	}

	/**
	 * Returns the email emitter
	 * 
	 * @param email
	 * @return emitter email if existent, empty string instead
	 * @throws MessagingException
	 */
	public static String getEmailEmitter(final Message email) throws MessagingException {
		Address[] fromAddresses = email.getFrom();
		String fromEmail = "";
		if (Objects.isNull(fromAddresses) || fromAddresses.length == 0) {
			LOGGER.info("The Sender isn't present!");
		} else {
			InternetAddress fromAddress = (InternetAddress) fromAddresses[0];
			fromEmail = fromAddress.getAddress();
		}
		return fromEmail;
	}

	/**
	 * Returns the email subject
	 * 
	 * @param email
	 * @return subject if existent, empty string instead
	 * @throws MessagingException
	 */
	public static String getEmailSubject(final Message email) throws MessagingException {
		String subject = email.getSubject();
		if (Objects.isNull(subject)) {
			subject = "";
		}
		return subject;
	}

	/**
	 * Returns the email attachments
	 * 
	 * @see https://stackoverflow.com/questions/1748183/download-attachments-using-java-mail
	 * 
	 * @param email
	 * @return The List of email attachments as BodyPart or empty List instead
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static List<BodyPart> getEmailAttachments(final Message email) throws IOException, MessagingException {
		List<BodyPart> attachments = new ArrayList<>();
		Multipart multipart = (Multipart) email.getContent();
		for (int i = 0; i < multipart.getCount(); i++) {
			BodyPart bodyPart = multipart.getBodyPart(i);
			// Only takes the attachments with name
			if (Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())) {
				attachments.add(bodyPart);
			}
		}
		return attachments;
	}

	/**
	 * Get email body content
	 * 
	 * @see https://stackoverflow.com/questions/11240368/how-to-read-text-inside-body-of-mail-using-javax-mail
	 * 
	 * @param message
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static String getBodyTextContent(final Message message) throws MessagingException, IOException {
		String result = "";
		if (message.isMimeType(MIME_TYPE_TXT)) {
			result = message.getContent().toString();
		} else if (message.isMimeType(MIME_TYPE_MULTIPART_ALL)) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	/**
	 * Get email body content if inside MultiPart
	 * 
	 * @param mimeMultipart
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	private static String getTextFromMimeMultipart(final MimeMultipart mimeMultipart)
			throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType(MIME_TYPE_TXT)) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType(MIME_TYPE_TXT_HTML)) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}
}
