package com.itesoft.test.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itesoft.test.models.Summary;

public class FilesHelper {

	private static final Logger LOGGER = Logger.getLogger(FilesHelper.class.getName());

	private static final ObjectMapper JSON_MAPPER = new ObjectMapper();

	public static final String PATH_SEPARATOR = "\\";
	public static final String TXT_EXTENSION = ".txt";

	private static final String SUMMARY_FILE_NAME = "summary.json";
	private static final String EML_EXTENSION = ".eml";

	/**
	 * Save the the attachment on the provided path with the following <i>format
	 * idEmail_nameAttachment.extensionAttachment</i>
	 * 
	 * <pre>
	 * 	<b> To write each attachment this method assume the fileName contains the extension name
	 * </pre>
	 * 
	 * @param basePath
	 *            The path where the attachment should be write
	 * @param attachment
	 *            The attachment to write
	 */
	public static void writeAttachment(final String basePath, final List<BodyPart> attachments) {
		for (BodyPart bodyPart : attachments) {
			FileOutputStream fos = null;
			try {
				if (ApplicationHelper.hasText(bodyPart.getFileName())) {
					InputStream is = bodyPart.getInputStream();
					File f = new File(basePath + "_" + bodyPart.getFileName());
					fos = new FileOutputStream(f);
					byte[] buf = new byte[4096];
					int bytesRead;
					while ((bytesRead = is.read(buf)) != -1) {
						fos.write(buf, 0, bytesRead);
					}
				} else {
					LOGGER.info("This attachment has not a valid name and will be ignored!");
				}
			} catch (MessagingException | IOException e) {
				LOGGER.log(Level.SEVERE, "unexpected method writing attachment", e);
			} finally {
				try {
					if (Objects.nonNull(fos)) {
						fos.close();
					}
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE, "unexpected exception closing the FileOutputStrem", e);
				}
			}
		}
	}

	/**
	 * Read the {@value #SUMMARY_FILE_NAME} if existing on provided
	 * {@code cleanPath} path and return his content if existent or empty
	 * {@code List<Summary>} if not
	 * 
	 * @param cleanPath
	 *            The path where file should be retrieved
	 * @return {@code List<Summary>} with the content file if existent, empty if non
	 *         existent, invalid or empty content
	 */
	public static List<Summary> getSummaryContent(final String cleanPath) {
		// Load summary file if exists
		File summaryFile = new File(cleanPath + PATH_SEPARATOR + SUMMARY_FILE_NAME);
		List<Summary> summaryList = new ArrayList<>();
		if (summaryFile.exists()) {
			try {
				summaryList = JSON_MAPPER.readValue(summaryFile, new TypeReference<List<Summary>>() {
				});
			} catch (JsonParseException e) {
				LOGGER.log(Level.SEVERE,
						"Error parsing the json file content!\n\tThe file can't be recovered and his content will be overrided!",
						e);
			} catch (JsonMappingException e) {
				LOGGER.log(Level.SEVERE,
						"Error mapping the json file content!\n\tThe file can't be recovered and his content will be overrided!",
						e);
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE,
						"Unexpected error reading the summary content!\\n\\tThe file can't be recovered and his content will be overrided!",
						e);
			}
		}
		return summaryList;
	}

	/**
	 * Write the elements of {@code summaryElements} to the
	 * {@value #SUMMARY_FILE_NAME} file of the provided path
	 * 
	 * @param basePath
	 *            The base path where the file should be write
	 * @param listOfSummarys
	 *            The content to write on json file
	 */
	public static void writeSummaryFile(final String basePath, final List<Summary> listOfSummarys) {
		try {
			JSON_MAPPER.writeValue(new File(basePath + PATH_SEPARATOR + SUMMARY_FILE_NAME), listOfSummarys);
		} catch (JsonGenerationException e) {
			LOGGER.log(Level.SEVERE, "Error generating the json file!\n\tThe file can't be writed!", e);
		} catch (JsonMappingException e) {
			LOGGER.log(Level.SEVERE, "Error mapping the json file content!\n\tThe file can't be writed!", e);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Unexpected error reading the summary content!\\n\\tThe file can't be writed!", e);
		}
	}

	/**
	 * Write the email on .eml format on the provided {@code filePath}
	 * 
	 * @param filePath
	 *            The file path where File should be write (with name but without
	 *            extension)
	 * @param email
	 *            The email to be write
	 */
	public static void writeEmlFile(final String filePath, final Message email) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(filePath + EML_EXTENSION);
			// write as .eml file
			email.writeTo(out);
		} catch (FileNotFoundException e) {
			LOGGER.log(Level.SEVERE, "Path for .eml not found!\\n\\tThe file can't be writed!", e);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Unexpected error writing the .eml file!\\n\\tThe file can't be writed!", e);
		} catch (MessagingException e) {
			LOGGER.log(Level.SEVERE, "Unexpected error reading email content!\\n\\tThe file can't be writed!", e);
		} finally {
			try {
				if (Objects.nonNull(out)) {
					out.close();
				}
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "unexpected exception closing the FileOutputStrem", e);
			}
		}
	}

	/**
	 * This method create a Text file on provided path {@code fileName} with subject and body text 
	 * existent on {@code email}
	 * 
	 * @param fileName The path to save the file
	 * @param email The email to retrieve the data
	 */
	public static void writeTxtFile(final String fileName, final Message email) {
		// Le sujet du mail + le corps dans un fichier TXT ( <num�ro>.txt)
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(fileName + TXT_EXTENSION, "UTF-8");
			writer.println("Subject:");
			writer.println(EmailHelper.getEmailSubject(email));
			writer.println("Body:");
			writer.println(EmailHelper.getBodyTextContent(email));
		} catch (FileNotFoundException e) {
			LOGGER.log(Level.SEVERE, "Path for .eml not found!\\n\\tThe file can't be writed!", e);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Unexpected error writing the .eml file!\\n\\tThe file can't be writed!", e);
		} catch (MessagingException e) {
			LOGGER.log(Level.SEVERE, "Unexpected error reading email content!\\n\\tThe file can't be writed!", e);
		} finally {
			if (Objects.nonNull(writer)) {
				writer.close();
			}
		}
	}

}
