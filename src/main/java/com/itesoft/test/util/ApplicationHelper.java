package com.itesoft.test.util;

import java.nio.file.Path;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class ApplicationHelper {
	
	private static final Logger LOGGER = Logger.getLogger(ApplicationHelper.class.getName());

	private static final String SANITIZE_EMAIL_REGEX = "[^a-zA-Z0-9\\.\\-]";
	private static final String SANITIZE_EMAIL_CHARACTER = "_";
	
	private static final String EMAIL_PATTERN_REGEX = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
	private static final Pattern EMAIL_PATTERN_VALIDATOR = Pattern.compile(EMAIL_PATTERN_REGEX, Pattern.CASE_INSENSITIVE);
	
	/**
	 * Convert provided Path to a Long value based on real file name
	 * 
	 * If parse isn't possible then 0 is returned
	 * 
	 * @param path The Path to convert
	 * @return The converted Long value
	 */
	public static Long mapToLong(final Path path) {
		final String realFileName = getFileNameFromPathWithoutExtension(path);
		Long result = 0L;
		try {
			result = Long.parseLong(realFileName);
		}catch (NumberFormatException e) {
			LOGGER.log(Level.INFO, "Error parsing name file to Long number", e);
		}
		return result;
	}

	/**
	 * Get the file part name without extension name by getting the filename from path and cutting te extension part 
	 * (at this point we know that the extension always is .txt)
	 * 
	 * @param path The Path containing the file name
	 * @return The file name
	 */
	private static String getFileNameFromPathWithoutExtension(final Path path) {
		return path.getFileName().toString().substring(0, path.getFileName().toString().length() - 4);
	}
	
	/**
	 * Check if path name ends with .txt extension file
	 * 
	 * @param path The Path to check
	 * @return boolean value
	 */
	public static boolean isTxtFile(final Path path) {
		return path.toString().endsWith(FilesHelper.TXT_EXTENSION);
	}
	
	/**
	 * Sanitize the provided email by replacing all the none authorized 
	 * chars based on {@value #SANITIZE_EMAIL_REGEX} regex by the {@value #SANITIZE_EMAIL_CHARACTER} char
	 * @param fromEmail The email to sanitize
	 * @return The sanitized email
	 */
	public static String sanitizeEmailFolderName(final String fromEmail) {
		return fromEmail.replaceAll(SANITIZE_EMAIL_REGEX, SANITIZE_EMAIL_CHARACTER);
	}

	/**
	 * This function check if the provided email is valid looking at this regex {@value #EMAIL_PATTERN_REGEX}
	 * 
	 * @param email The email to check
	 * @return true if valid, false instead
	 */
	public static boolean isValidEmail(final String email) {
		return EMAIL_PATTERN_VALIDATOR.matcher(email).matches();
	}
	
	/**
	 * This method check if the provided String contains text (remove spaces)
	 * 
	 * @param stringToCheck The String to check
	 * @return True is has text, false if not
	 */
	public static boolean hasText(final String stringToCheck) {
		if (Objects.isNull(stringToCheck) || stringToCheck.trim().isEmpty()) {
			return false;
		}
		return true;
	}
}
